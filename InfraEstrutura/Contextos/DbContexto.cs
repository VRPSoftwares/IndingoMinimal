﻿using Dominio;
using Dominio.Entidades;
using InfraEstrutura.Configuracoes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace InfraEstrutura.Contextos
{
    public class DbContexto(DbContextOptions<DbContexto> options, IConfiguration configuration) : DbContext(options)
    {
        private readonly IConfiguration _configuration = configuration;

        //public DbContexto()
        //{

        //}

        public DbSet<Agencias> Agencias { get; set; }
        public DbSet<Bancos> Bancos { get; set; }
        public DbSet<ContasBancaria> ContasBancarias { get; set; }
        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Pessoas> Pessoas { get; set; }
        public DbSet<PessoaFisica> PessoaFisica { get; set; }
        public DbSet<PessoaJuridica> PessoaJuridica { get; set; }

        //public DbContexto(IConfiguration configuration) 
        //{
        //    _configuration = configuration;
        //}

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            try
            {
                if (options.Options.Extensions.LastOrDefault() is null)
                    options.UseSqlServer(_configuration.GetConnectionString("SQLServer"));
                else
                {
                    var inMemory = options.Options.Extensions.LastOrDefault().Info.Extension.ToString().Contains("Microsoft.EntityFrameworkCore.InMemory.Infrastructure.Internal.InMemoryOptionsExtension");

                    if (!inMemory)
                        options.UseSqlServer(_configuration.GetConnectionString("SQLServer"));
                }
            }
            catch
            {
                options.UseSqlServer(_configuration.GetConnectionString("SQLServer"));
            }

            options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ApplyConfigurations(modelBuilder);
            ApplyEntity(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                modelBuilder.Entity(entityType.ClrType).Property<DateTime>(nameof(EntidadeBase.DataCadastro)).ValueGeneratedOnAddOrUpdate().HasDefaultValueSql("getdate()").Metadata.SetBeforeSaveBehavior(PropertySaveBehavior.Ignore);
                modelBuilder.Entity(entityType.ClrType).Property<DateTime>(nameof(EntidadeBase.DataAlteracao)).ValueGeneratedOnUpdateSometimes().HasDefaultValueSql("getdate()").Metadata.SetBeforeSaveBehavior(PropertySaveBehavior.Ignore);
            }

            base.OnModelCreating(modelBuilder);
        }

        private static void ApplyConfigurations(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AgenciasConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new BancosConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new ContasBancariasConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new PessoasConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new PessoaFisicaConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new PessoaJuridicaConfiguracaoEntidade());
            modelBuilder.ApplyConfiguration(new UsuariosConfiguracaoEntidade());
        }

        private static void ApplyEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bancos>().Navigation(e => e.Agencias).AutoInclude();
            modelBuilder.Entity<Agencias>().Navigation(e => e.Banco).AutoInclude();
            modelBuilder.Entity<ContasBancaria>().Navigation(e => e.Agencias).AutoInclude();
            modelBuilder.Entity<PessoaJuridica>().Navigation(e => e.Pessoa).AutoInclude();
            modelBuilder.Entity<PessoaFisica>().Navigation(e => e.Pessoa).AutoInclude();
        }
    }
}
