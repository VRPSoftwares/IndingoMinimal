﻿BEGIN TRANSACTION;
GO

CREATE TABLE [Usuarios] (
    [UsuarioId] uniqueidentifier NOT NULL,
    [Nome] varchar(100) NOT NULL,
    [Email] varchar(100) NOT NULL,
    [Senha] varchar(250) NULL,
    [Ativo] bit NOT NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_Usuarios] PRIMARY KEY ([UsuarioId])
);
GO

CREATE UNIQUE INDEX [IX_Usuarios_Email] ON [Usuarios] ([Email]);
GO

CREATE UNIQUE INDEX [IX_Usuarios_Nome] ON [Usuarios] ([Nome]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20240414153835_Usuarios', N'8.0.3');
GO

COMMIT;
GO

