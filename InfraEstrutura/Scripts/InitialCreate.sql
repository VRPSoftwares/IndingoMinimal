﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Bancos] (
    [BancoId] uniqueidentifier NOT NULL,
    [Descricao] varchar(100) NOT NULL,
    [Codigo] varchar(3) NOT NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_Bancos] PRIMARY KEY ([BancoId])
);
GO

CREATE TABLE [Agencias] (
    [AgenciaId] uniqueidentifier NOT NULL,
    [BancoId] uniqueidentifier NOT NULL,
    [Descricao] varchar(100) NOT NULL,
    [NumeroAgencia] varchar(10) NOT NULL,
    [DigitoAgencia] varchar(5) NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_Agencias] PRIMARY KEY ([AgenciaId]),
    CONSTRAINT [FK_Agencias_Bancos_BancoId] FOREIGN KEY ([BancoId]) REFERENCES [Bancos] ([BancoId]) ON DELETE CASCADE
);
GO

CREATE TABLE [ContasBancarias] (
    [ContaBancariaId] uniqueidentifier NOT NULL,
    [AgenciaId] uniqueidentifier NOT NULL,
    [Descricao] varchar(100) NOT NULL,
    [NumeroConta] varchar(10) NOT NULL,
    [DigitoConta] varchar(5) NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_ContasBancarias] PRIMARY KEY ([ContaBancariaId]),
    CONSTRAINT [FK_ContasBancarias_Agencias_AgenciaId] FOREIGN KEY ([AgenciaId]) REFERENCES [Agencias] ([AgenciaId]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Agencias_BancoId] ON [Agencias] ([BancoId]);
GO

CREATE UNIQUE INDEX [IX_Agencias_Descricao] ON [Agencias] ([Descricao]);
GO

CREATE UNIQUE INDEX [IX_Agencias_NumeroAgencia_BancoId] ON [Agencias] ([NumeroAgencia], [BancoId]);
GO

CREATE UNIQUE INDEX [IX_Bancos_Codigo] ON [Bancos] ([Codigo]);
GO

CREATE UNIQUE INDEX [IX_Bancos_Descricao] ON [Bancos] ([Descricao]);
GO

CREATE INDEX [IX_ContasBancarias_AgenciaId] ON [ContasBancarias] ([AgenciaId]);
GO

CREATE INDEX [IX_ContasBancarias_Descricao_NumeroConta] ON [ContasBancarias] ([Descricao], [NumeroConta]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20240410214621_InitialCreate', N'8.0.3');
GO

COMMIT;
GO

