﻿BEGIN TRANSACTION;
GO

CREATE TABLE [PessoaFisica] (
    [PessoaFisicaId] uniqueidentifier NOT NULL,
    [PessoaId] uniqueidentifier NULL,
    [Nome] varchar(100) NOT NULL,
    [Apelido] varchar(100) NULL,
    [DataNascimento] date NOT NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_PessoaFisica] PRIMARY KEY ([PessoaFisicaId])
);
GO

CREATE TABLE [PessoaJuridica] (
    [PessoaJuridicaId] uniqueidentifier NOT NULL,
    [PessoaId] uniqueidentifier NULL,
    [RazaoSocial] varchar(100) NOT NULL,
    [NomeFantasia] varchar(100) NULL,
    [DataFundacao] date NOT NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_PessoaJuridica] PRIMARY KEY ([PessoaJuridicaId])
);
GO

CREATE TABLE [Pessoas] (
    [PessoaId] uniqueidentifier NOT NULL,
    [TipoPessoa] int NOT NULL,
    [TipoCadastro] int NOT NULL,
    [Documento] varchar(20) NOT NULL,
    [Email] varchar(100) NOT NULL,
    [DataCadastro] datetime2 NOT NULL DEFAULT (getdate()),
    [DataAlteracao] datetime2 NOT NULL DEFAULT (getdate()),
    CONSTRAINT [PK_Pessoas] PRIMARY KEY ([PessoaId]),
    CONSTRAINT [FK_Pessoas_PessoaFisica_PessoaId] FOREIGN KEY ([PessoaId]) REFERENCES [PessoaFisica] ([PessoaFisicaId]) ON DELETE CASCADE,
    CONSTRAINT [FK_Pessoas_PessoaJuridica_PessoaId] FOREIGN KEY ([PessoaId]) REFERENCES [PessoaJuridica] ([PessoaJuridicaId]) ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX [IX_PessoaFisica_PessoaId] ON [PessoaFisica] ([PessoaId]) WHERE [PessoaId] IS NOT NULL;
GO

CREATE UNIQUE INDEX [IX_PessoaJuridica_PessoaId] ON [PessoaJuridica] ([PessoaId]) WHERE [PessoaId] IS NOT NULL;
GO

CREATE UNIQUE INDEX [IX_Pessoas_Documento] ON [Pessoas] ([Documento]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20240425111856_Pessoas', N'8.0.3');
GO

COMMIT;
GO

