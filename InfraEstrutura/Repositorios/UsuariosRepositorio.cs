﻿using Dominio.Entidades;
using InfraEstrutura.Contextos;
using InfraEstrutura.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace InfraEstrutura.Repositorios
{
    public class UsuariosRepositorio(DbContexto context) : IUsuariosRepositorios
    {
        private readonly DbContexto _context = context ?? throw new ArgumentNullException(nameof(context));

        public async Task<IEnumerable<Usuarios>> ObterTodosAsync()
        {
            return await _context.Usuarios.ToListAsync();
        }

        public async Task<Usuarios> ObterAsync(Guid Id)
        {
            return await _context.Usuarios.FindAsync(Id) ?? throw new Exception("Nenhum registro encontrado");
        }

        public async Task<IEnumerable<Usuarios>> ObterFiltroAsync(Expression<Func<Usuarios, bool>> filtro)
        {
            return await Task.FromResult(_context.Usuarios.Where(filtro).AsEnumerable());
        }

        public async Task<Usuarios> CriarAsync(Usuarios usuarios)
        {
            usuarios.UsuarioId = Guid.NewGuid();
            await _context.Usuarios.AddAsync(usuarios);
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)usuarios.UsuarioId);
        }

        public async Task<Usuarios> AtualizarAsync(Usuarios usuarios)
        {
            _context.Entry(usuarios).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)usuarios.UsuarioId!);
        }

        public async Task<bool> RemoverAsync(Guid Id)
        {
            var dado = await _context.Usuarios.FindAsync(Id) ?? throw new Exception("Registro não localizado");
            _context.Entry(dado).State = EntityState.Deleted;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
