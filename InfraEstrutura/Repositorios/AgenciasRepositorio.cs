﻿using Dominio.Entidades;
using InfraEstrutura.Contextos;
using InfraEstrutura.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace InfraEstrutura.Repositorios
{
    public class AgenciasRepositorio(DbContexto context) : IAgenciasRepositorio
    {
        private readonly DbContexto _context = context ?? throw new ArgumentNullException(nameof(context));

        public async Task<IEnumerable<Agencias>> ObterTodosAsync()
        {
            return await _context.Agencias.ToListAsync();
        }

        public async Task<Agencias> ObterAsync(Guid Id)
        {
            return await _context.Agencias.FindAsync(Id) ?? throw new Exception("Nenhum registro encontrado");
        }

        public async Task<IEnumerable<Agencias>> ObterFiltroAsync(Expression<Func<Agencias, bool>> filtro)
        {
            return await Task.FromResult(_context.Agencias.Where(filtro).AsEnumerable());
        }

        public async Task<Agencias> CriarAsync(Agencias agencia)
        {
            agencia.AgenciaId = Guid.NewGuid();
            await _context.Agencias.AddAsync(agencia);
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)agencia.AgenciaId!);
        }

        public async Task<Agencias> AtualizarAsync(Agencias agencia)
        {
            _context.Entry(agencia).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)agencia.AgenciaId!);
        }

        public async Task<bool> RemoverAsync(Guid Id)
        {
            var dado = await _context.Agencias.FindAsync(Id) ?? throw new Exception("Registro não localizado");
            _context.Entry(dado).State = EntityState.Deleted;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
