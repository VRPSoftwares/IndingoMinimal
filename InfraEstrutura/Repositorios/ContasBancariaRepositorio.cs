﻿using Dominio.Entidades;
using InfraEstrutura.Contextos;
using InfraEstrutura.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace InfraEstrutura.Repositorios
{
    public class ContasBancariasRepositorio(DbContexto context) : IContasBancariasRepositorio
    {
        private readonly DbContexto _context = context ?? throw new ArgumentNullException(nameof(context));

        public async Task<IEnumerable<ContasBancaria>> ObterTodosAsync()
        {
            return await _context.ContasBancarias.ToListAsync();
        }

        public async Task<ContasBancaria> ObterAsync(Guid Id)
        {
            return await _context.ContasBancarias.FindAsync(Id) ?? throw new Exception("Nenhum registro encontrado");
        }

        public async Task<IEnumerable<ContasBancaria>> ObterFiltroAsync(Expression<Func<ContasBancaria, bool>> filtro)
        {
            return await Task.FromResult(_context.ContasBancarias.Where(filtro).AsEnumerable());
        }

        public async Task<ContasBancaria> CriarAsync(ContasBancaria contasBancaria)
        {
            contasBancaria.ContaBancariaId = Guid.NewGuid();
            await _context.ContasBancarias.AddAsync(contasBancaria);
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)contasBancaria.ContaBancariaId);
        }

        public async Task<ContasBancaria> AtualizarAsync(ContasBancaria contasBancaria)
        {
            _context.Entry(contasBancaria).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)contasBancaria.ContaBancariaId!);
        }

        public async Task<bool> RemoverAsync(Guid Id)
        {
            var dado = await _context.Bancos.FindAsync(Id) ?? throw new Exception("Registro não localizado");
            _context.Entry(dado).State = EntityState.Deleted;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
