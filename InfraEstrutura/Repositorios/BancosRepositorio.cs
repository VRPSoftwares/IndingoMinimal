﻿using Dominio.Entidades;
using InfraEstrutura.Contextos;
using InfraEstrutura.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace InfraEstrutura.Repositorios
{
    public class BancosRepositorio(DbContexto context) : IBancosRepositorio
    {
        private readonly DbContexto _context = context ?? throw new ArgumentNullException(nameof(context));

        public async Task<IEnumerable<Bancos>> ObterTodosAsync()
        {
            return await _context.Bancos.ToListAsync();
        }

        public async Task<Bancos> ObterAsync(Guid Id)
        {
            return await _context.Bancos.FindAsync(Id)??throw new Exception("Nenhum registro encontrado");
        }

        public async Task<IEnumerable<Bancos>> ObterFiltroAsync(Expression<Func<Bancos, bool>> filtro)
        {
            return await Task.FromResult(_context.Bancos.Where(filtro).AsEnumerable());
        }

        public async Task<Bancos> CriarAsync(Bancos bancos)
        {
            bancos.BancoId = Guid.NewGuid();
            await _context.Bancos.AddAsync(bancos);
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)bancos.BancoId);
        }

        public async Task<Bancos> AtualizarAsync(Bancos bancos)
        {
            _context.Entry(bancos).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return await ObterAsync((Guid)bancos.BancoId!);
        }

        public async Task<bool> RemoverAsync(Guid Id)
        {
            var dado = await _context.Bancos.FindAsync(Id) ?? throw new Exception("Registro não localizado");
            _context.Entry(dado).State = EntityState.Deleted;
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
