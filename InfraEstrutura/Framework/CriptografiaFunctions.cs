﻿using System.Security.Cryptography;
using System.Text;

namespace InfraEstrutura.Framework
{
    public static class CriptografiaFunctions
    {
        public static class CriptoAES256
        {
            public static string DecryptStringAES(string cipherText, string keybytes)
            {
                var KeyBytes = Encoding.UTF8.GetBytes(keybytes);
                var iv = Encoding.UTF8.GetBytes("4fdE32Wnj7Kgt631");

                var encrypted = Convert.FromBase64String(cipherText);
                var decriptedFromJavascript = DecryptStringFromBytes(encrypted, KeyBytes, iv);
                return decriptedFromJavascript;
            }
            private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
            {
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");
                if (key == null || key.Length <= 0)
                    throw new ArgumentNullException("key");
                if (iv == null || iv.Length <= 0)
                    throw new ArgumentNullException("key");

                string? plaintext = null;
                
                using (var rijAlg = new RijndaelManaged())
                {
                    rijAlg.Mode = CipherMode.CBC;
                    rijAlg.Padding = PaddingMode.PKCS7;
                    rijAlg.FeedbackSize = 128;
                    rijAlg.Key = key;
                    rijAlg.IV = iv;

                    var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    try
                    {
                        using (var msDecrypt = new MemoryStream(cipherText))
                        {
                            using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                            {

                                using (var srDecrypt = new StreamReader(csDecrypt))
                                {
                                    plaintext = srDecrypt.ReadToEnd();

                                }
                            }
                        }
                    }
                    catch
                    {
                        plaintext = "keyError";
                    }
                }

                return plaintext;
            }

            public static string EncryptStringAES(string plainText, string keybytes)
            {
                var KeyBytes = Encoding.UTF8.GetBytes(keybytes);
                var iv = Encoding.UTF8.GetBytes("4fdE32Wnj7Kgt631");
                var encryoFromJavascript = EncryptStringToBytes(plainText, KeyBytes, iv);
                return Convert.ToBase64String(encryoFromJavascript);
            }

            private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
            {
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException(nameof(plainText));
                if (key == null || key.Length <= 0)
                    throw new ArgumentNullException(nameof(key));
                if (iv == null || iv.Length <= 0)
                    throw new ArgumentNullException(nameof(key));
                
                byte[] encrypted;
                using (var rijAlg = new RijndaelManaged())
                {
                    rijAlg.Mode = CipherMode.CBC;
                    rijAlg.Padding = PaddingMode.PKCS7;
                    rijAlg.FeedbackSize = 128;

                    rijAlg.Key = key;
                    rijAlg.IV = iv;

                    var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }
                
                return encrypted;
            }
        }
    }
}
