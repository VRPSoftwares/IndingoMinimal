﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace InfraEstrutura.Framework
{
    public static class TokenFunctions
    {
        public static string GerarTokenJwt(IConfiguration _configuration, Guid UsuarioId, string Name, string Email)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Sid, UsuarioId.ToString().ToUpper()),
                new(ClaimTypes.Name, Name),
                new(ClaimTypes.Email, Email)
            };

            var issuer = _configuration["Jwt:Issuer"];
            var audience = _configuration["Jwt:Audience"];
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(claims: claims, issuer: issuer, audience: audience, expires: DateTime.UtcNow.AddYears(50), notBefore: DateTime.UtcNow, signingCredentials: credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);
            return stringToken;
        }
    }
}
