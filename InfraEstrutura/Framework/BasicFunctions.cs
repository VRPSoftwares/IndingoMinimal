﻿using System.Text;

namespace InfraEstrutura.Framework
{
    public static class BasicFunctions
    {
        private static readonly Random random = new Random();

        public static string GenerateRandomPassword(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            StringBuilder passwordBuilder = new();

            for (int i = 0; i < length; i++)
            {
                int index = random.Next(chars.Length);
                passwordBuilder.Append(chars[index]);
            }

            return passwordBuilder.ToString();
        }
    }
}
