﻿using Dominio.Entidades;
using System.Linq.Expressions;

namespace InfraEstrutura.Interfaces.Repositorios
{
    public interface IPessoasRepositorio
    {
        Task<IEnumerable<Pessoas>> ObterTodosAsync();
        Task<Pessoas> ObterAsync(Guid Id);
        Task<IEnumerable<Pessoas>> ObterFiltroAsync(Expression<Func<Pessoas, bool>> filtro);
        Task<Pessoas> CriarAsync(Pessoas agencia);
        Task<Pessoas> AtualizarAsync(Pessoas agencia);
        Task<bool> RemoverAsync(Guid Id);
    }
}
