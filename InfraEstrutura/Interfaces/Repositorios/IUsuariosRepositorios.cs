﻿using Dominio.Entidades;
using System.Linq.Expressions;

namespace InfraEstrutura.Interfaces.Repositorios
{
    public interface IUsuariosRepositorios
    {
        Task<IEnumerable<Usuarios>> ObterTodosAsync();
        Task<Usuarios> ObterAsync(Guid Id);
        Task<IEnumerable<Usuarios>> ObterFiltroAsync(Expression<Func<Usuarios, bool>> filtro);
        Task<Usuarios> CriarAsync(Usuarios bancos);
        Task<Usuarios> AtualizarAsync(Usuarios bancos);
        Task<bool> RemoverAsync(Guid Id);
    }
}
