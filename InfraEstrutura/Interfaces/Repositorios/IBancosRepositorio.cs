﻿using Dominio.Entidades;
using System.Linq.Expressions;

namespace InfraEstrutura.Interfaces.Repositorios
{
    public interface IBancosRepositorio
    {
        Task<IEnumerable<Bancos>> ObterTodosAsync();
        Task<Bancos> ObterAsync(Guid Id);
        Task<IEnumerable<Bancos>> ObterFiltroAsync(Expression<Func<Bancos, bool>> filtro);
        Task<Bancos> CriarAsync(Bancos bancos);
        Task<Bancos> AtualizarAsync(Bancos bancos);
        Task<bool> RemoverAsync(Guid Id);
    }
}
