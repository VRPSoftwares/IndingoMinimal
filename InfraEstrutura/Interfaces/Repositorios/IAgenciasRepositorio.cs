﻿using Dominio.Entidades;
using System.Linq.Expressions;

namespace InfraEstrutura.Interfaces.Repositorios
{
    public interface IAgenciasRepositorio
    {
        Task<IEnumerable<Agencias>> ObterTodosAsync();
        Task<Agencias> ObterAsync(Guid Id);
        Task<IEnumerable<Agencias>> ObterFiltroAsync(Expression<Func<Agencias, bool>> filtro);
        Task<Agencias> CriarAsync(Agencias agencia);
        Task<Agencias> AtualizarAsync(Agencias agencia);
        Task<bool> RemoverAsync(Guid Id);
    }
}
