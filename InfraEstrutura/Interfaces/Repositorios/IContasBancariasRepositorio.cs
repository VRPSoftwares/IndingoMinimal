﻿using Dominio.Entidades;
using System.Linq.Expressions;

namespace InfraEstrutura.Interfaces.Repositorios
{
    public interface IContasBancariasRepositorio
    {
        Task<IEnumerable<ContasBancaria>> ObterTodosAsync();
        Task<ContasBancaria> ObterAsync(Guid Id);
        Task<IEnumerable<ContasBancaria>> ObterFiltroAsync(Expression<Func<ContasBancaria, bool>> filtro);
        Task<ContasBancaria> CriarAsync(ContasBancaria bancos);
        Task<ContasBancaria> AtualizarAsync(ContasBancaria bancos);
        Task<bool> RemoverAsync(Guid Id);
    }
}
