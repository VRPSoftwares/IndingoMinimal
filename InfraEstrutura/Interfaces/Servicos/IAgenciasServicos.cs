﻿using Dominio.Entidades;
using System.Linq.Expressions;
using static Dominio.Models.AgenciasModels;

namespace InfraEstrutura.Interfaces.Servicos
{
    public interface IAgenciasServicos
    {
        Task<IEnumerable<SelectAgencia>> ObterTodosAsync();
        Task<SelectAgencia> ObterAsync(Guid Id);
        Task<IEnumerable<SelectAgencia>> ObterFiltroAsync(Expression<Func<Agencias, bool>> filtro);
        Task<(SelectAgencia, IEnumerable<string>)> CriarAsync(InsertAgencia insert);
        Task<(SelectAgencia, IEnumerable<string>)> AtualizarAsync(UpdateAgencia update);
        Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id);
    }
}
