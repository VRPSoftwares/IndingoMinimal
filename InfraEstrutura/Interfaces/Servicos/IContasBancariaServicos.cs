﻿using Dominio.Entidades;
using System.Linq.Expressions;
using static Dominio.Models.ContasBancariaModels;

namespace InfraEstrutura.Interfaces.Servicos
{
    public interface IContasBancariaServicos
    {
        Task<IEnumerable<SelectContasBancarias>> ObterTodosAsync();
        Task<SelectContasBancarias> ObterAsync(Guid Id);
        Task<IEnumerable<SelectContasBancarias>> ObterFiltroAsync(Expression<Func<ContasBancaria, bool>> filtro);
        Task<(SelectContasBancarias, IEnumerable<string>)> CriarAsync(InsertContasBancarias insert);
        Task<(SelectContasBancarias, IEnumerable<string>)> AtualizarAsync(UpdateContasBancarias update);
        Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id);
    }
}
