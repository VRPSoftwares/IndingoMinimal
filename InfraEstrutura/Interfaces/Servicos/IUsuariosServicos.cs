﻿using Dominio.Entidades;
using System.Linq.Expressions;
using static Dominio.Models.UsuariosModels;

namespace InfraEstrutura.Interfaces.Servicos
{
    public interface IUsuariosServicos
    {
        Task<IEnumerable<SelectUsuario>> ObterTodosAsync();
        Task<SelectUsuario> ObterAsync(Guid Id);
        Task<IEnumerable<SelectUsuario>> ObterFiltroAsync(Expression<Func<Usuarios, bool>> filtro);
        Task<(SelectUsuario, IEnumerable<string>)> CriarAsync(InsertUsuario insert);
        Task<(SelectUsuario, IEnumerable<string>)> AtualizarAsync(UpdateUsuario update);
        Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id);
        Task<(LoginUsuarioReturn, IEnumerable<string>)> CheckLoginAsync(LoginUsuario login);
    }
}
