﻿using Dominio.Entidades;
using System.Linq.Expressions;
using static Dominio.Models.BancosModels;

namespace InfraEstrutura.Interfaces.Servicos
{
    public interface IBancosServicos
    {
        Task<IEnumerable<SelectBanco>> ObterTodosAsync();
        Task<SelectBanco> ObterAsync(Guid Id);
        Task<IEnumerable<SelectBanco>> ObterFiltroAsync(Expression<Func<Bancos, bool>> filtro);
        Task<(SelectBanco, IEnumerable<string>)> CriarAsync(InsertBanco insert);
        Task<(SelectBanco, IEnumerable<string>)> AtualizarAsync(UpdateBanco update);
        Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id);
    }
}
