﻿using Dominio.Entidades;
using System.Linq.Expressions;
using static Dominio.Models.PessoasModels;

namespace InfraEstrutura.Interfaces.Servicos
{
    public interface IPessoasServicos
    {
        Task<IEnumerable<SelectPessoas>> ObterTodosAsync();
        Task<SelectPessoas> ObterAsync(Guid Id);
        Task<IEnumerable<SelectPessoas>> ObterFiltroAsync(Expression<Func<Pessoas, bool>> filtro);
        Task<(SelectPessoas, IEnumerable<string>)> CriarAsync(InsertPessoas insert);
        Task<(SelectPessoas, IEnumerable<string>)> AtualizarAsync(UpdatePessoas update);
        Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id);
    }
}
