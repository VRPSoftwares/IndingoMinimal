﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace InfraEstrutura.Configuracoes
{
    public class PessoasConfiguracaoEntidade : IEntityTypeConfiguration<Pessoas>
    {
        public void Configure(EntityTypeBuilder<Pessoas> builder)
        {
            builder.ToTable("Pessoas");
            builder.HasKey(c => c.PessoaId);
            builder.Property(c => c.TipoPessoa).IsRequired().HasColumnType("int");
            builder.Property(c => c.TipoCadastro).IsRequired().HasColumnType("int");
            builder.Property(c => c.Documento).IsRequired().HasColumnType("varchar(20)");
            builder.Property(c => c.Email).IsRequired().HasColumnType("varchar(100)");
            
            builder.HasIndex(c => c.Documento).IsUnique();
        }
    }
}
