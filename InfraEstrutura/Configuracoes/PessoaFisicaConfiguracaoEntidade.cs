﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class PessoaFisicaConfiguracaoEntidade : IEntityTypeConfiguration<PessoaFisica>
    {
        public void Configure(EntityTypeBuilder<PessoaFisica> builder)
        {
            builder.ToTable("PessoaFisica");
            builder.HasKey(c => c.PessoaFisicaId);      
            builder.Property(c => c.Apelido).HasColumnType("varchar(100)");
            builder.Property(c => c.DataNascimento).IsRequired().HasColumnType("date");
            builder.Property(c => c.Nome).IsRequired().HasColumnType("varchar(100)");

            builder.HasOne(e => e.Pessoa)
                .WithOne(e => e.PessoaFisica)
                .HasForeignKey<Pessoas>(e => e.PessoaId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(c => c.PessoaId).IsUnique();
        }
    }
}
