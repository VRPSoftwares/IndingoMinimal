﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class ContasBancariasConfiguracaoEntidade : IEntityTypeConfiguration<ContasBancaria>
    {
        public void Configure(EntityTypeBuilder<ContasBancaria> builder)
        {
            builder.ToTable("ContasBancarias");
            builder.HasKey(c => c.ContaBancariaId);
            builder.Property(c => c.Descricao).IsRequired().HasColumnType("varchar(100)");
            builder.Property(c => c.NumeroConta).IsRequired().HasColumnType("varchar(10)");
            builder.Property(c => c.DigitoConta).HasColumnType("varchar(5)");

            builder.HasOne(e => e.Agencias)
                .WithMany(e => e.ContasBancarias)
                .HasForeignKey(e => e.AgenciaId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(c => new { c.Descricao, c.NumeroConta });
        }
    }
}