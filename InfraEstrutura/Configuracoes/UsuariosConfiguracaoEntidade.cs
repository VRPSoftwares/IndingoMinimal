﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class UsuariosConfiguracaoEntidade : IEntityTypeConfiguration<Usuarios>
    {
        public void Configure(EntityTypeBuilder<Usuarios> builder)
        {
            builder.ToTable("Usuarios");
            builder.HasKey(c => c.UsuarioId);
            builder.Property(c => c.Nome).IsRequired().HasColumnType("varchar(100)");
            builder.Property(c => c.Email).IsRequired().HasColumnType("varchar(100)");
            builder.Property(c => c.Senha).HasColumnType("varchar(250)");

            builder.HasIndex(c => c.Email).IsUnique();
            builder.HasIndex(c => c.Nome).IsUnique();
        }
    }
}
