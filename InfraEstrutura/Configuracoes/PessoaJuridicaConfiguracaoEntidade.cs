﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class PessoaJuridicaConfiguracaoEntidade : IEntityTypeConfiguration<PessoaJuridica>
    {
        public void Configure(EntityTypeBuilder<PessoaJuridica> builder)
        {
            builder.ToTable("PessoaJuridica");
            builder.HasKey(c => c.PessoaJuridicaId);
            builder.Property(c => c.NomeFantasia).HasColumnType("varchar(100)");
            builder.Property(c => c.DataFundacao).IsRequired().HasColumnType("date");
            builder.Property(c => c.RazaoSocial).IsRequired().HasColumnType("varchar(100)");

            builder.HasOne(e => e.Pessoa)
                .WithOne(e => e.PessoaJuridica)
                .HasForeignKey<Pessoas>(e => e.PessoaId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(c => c.PessoaId).IsUnique();
        }
    }
}
