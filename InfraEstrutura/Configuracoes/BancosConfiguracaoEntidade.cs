﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class BancosConfiguracaoEntidade : IEntityTypeConfiguration<Bancos>
    {
        public void Configure(EntityTypeBuilder<Bancos> builder)
        {
            builder.ToTable("Bancos");
            builder.HasKey(c => c.BancoId);
            builder.Property(c => c.Descricao).IsRequired().HasColumnType("varchar(100)");
            builder.Property(c => c.Codigo).IsRequired().HasColumnType("varchar(3)");

            builder.HasIndex(c => c.Descricao).IsUnique();
            builder.HasIndex(c => c.Codigo).IsUnique();
        }
    }
}
