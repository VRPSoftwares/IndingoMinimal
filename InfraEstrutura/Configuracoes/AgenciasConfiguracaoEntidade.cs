﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfraEstrutura.Configuracoes
{
    public class AgenciasConfiguracaoEntidade : IEntityTypeConfiguration<Agencias>
    {
        public void Configure(EntityTypeBuilder<Agencias> builder)
        {
            builder.ToTable("Agencias");
            builder.HasKey(c => c.AgenciaId);
            builder.Property(c => c.Descricao).IsRequired().HasColumnType("varchar(100)");
            builder.Property(c => c.NumeroAgencia).IsRequired().HasColumnType("varchar(10)");
            builder.Property(c => c.DigitoAgencia).HasColumnType("varchar(5)");

            builder.HasOne(e => e.Banco)
                .WithMany(e => e.Agencias)
                .HasForeignKey(e => e.BancoId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);


            builder.HasIndex(c => c.Descricao).IsUnique();
            builder.HasIndex(c => new { c.NumeroAgencia, c.BancoId }).IsUnique();
        }
    }
}
