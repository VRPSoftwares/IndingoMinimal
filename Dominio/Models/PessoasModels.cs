﻿using AutoMapper;
using Dominio.Entidades;
using Dominio.Enums;

namespace Dominio.Models
{
    public class PessoasModels
    {
        [AutoMap(typeof(Entidades.Pessoas), ReverseMap = true)]
        public class SelectPessoas
        {
            public Guid? PessoaId { get; set; }
            public TipoPessoa TipoPessoa { get; set; }
            public TipoCadastro TipoCadastro { get; set; }
            public string? Documento { get; set; }
            public string? Email { get; set; }
            public PessoaFisica? PessoaFisica { get; set; }
            public PessoaJuridica? PessoaJuridica { get; set; }
            public DateTime DataAlteracao { get; set; }
        }

        public class InsertPessoas
        {
            public TipoPessoa TipoPessoa { get; set; }
            public TipoCadastro TipoCadastro { get; set; }
            public string? Documento { get; set; }
            public string? Email { get; set; }
        }

        public class UpdatePessoas
        {
            public Guid? PessoaId { get; set; }
            public TipoPessoa TipoPessoa { get; set; }
            public TipoCadastro TipoCadastro { get; set; }
            public string? Documento { get; set; }
            public string? Email { get; set; }
        }
    }
}
