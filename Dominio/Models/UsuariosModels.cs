﻿using AutoMapper;

namespace Dominio.Models
{
    public class UsuariosModels
    {
        [AutoMap(typeof(Dominio.Entidades.Usuarios), ReverseMap = true)]
        public class SelectUsuario
        {
            public Guid? UsuarioId { get; set; }
            public string? Nome { get; set; }
            public string? Email{ get; set; }
            public bool Ativo { get; set; }
            public DateTime DataAlteracao { get; set; }
        }

        public class InsertUsuario
        {
            public string? Nome { get; set; }
            public string? Email { get; set; }
            public bool Ativo { get; set; }
        }

        public class UpdateUsuario
        {
            public Guid? UsuarioId { get; set; }
            public string? Nome { get; set; }
            public string? Email { get; set; }
            public bool Ativo { get; set; }
        }

        public class LoginUsuario
        {
            public string? Email { get; set; }
            public string? Password { get; set; }
        }

        public class LoginUsuarioReturn
        {
            public string? UsuarioId { get; set; }
            public string? Nome { get; set; }
            public string? Email { get; set; }
            public string? Token { get; set; }
        }
    }
}
