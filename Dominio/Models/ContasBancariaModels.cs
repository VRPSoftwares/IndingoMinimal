﻿using AutoMapper;

namespace Dominio.Models
{
    public class ContasBancariaModels
    {
        [AutoMap(typeof(Dominio.Entidades.ContasBancaria), ReverseMap = true)]
        public class SelectContasBancarias
        {
            public Guid? ContaBancariaId { get; set; }
            public Guid? AgenciaId { get; set; }
            public Guid? BancoId { get; set; }
            public string? Agencia { get; set; }
            public string? NomeBanco { get; set; }
            public string? CodigoBanco { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroConta { get; set; }
            public string? DigitoConta { get; set; }
            public DateTime DataAlteracao { get; set; }
        }

        public class InsertContasBancarias
        {
            public Guid? AgenciaId { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroConta { get; set; }
            public string? DigitoConta { get; set; }
        }

        public class UpdateContasBancarias
        {
            public Guid? ContaBancariaId { get; set; }
            public Guid? AgenciaId { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroConta { get; set; }
            public string? DigitoConta { get; set; }
        }
    }
}
