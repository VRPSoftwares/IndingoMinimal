﻿using AutoMapper;

namespace Dominio.Models
{
    public class BancosModels
    {
        [AutoMap(typeof(Entidades.Bancos), ReverseMap = true)]
        public class SelectBanco
        {
            public Guid? BancoId { get; set; }
            public string? Descricao { get; set; }
            public string? Codigo { get; set; }
            public DateTime DataAlteracao { get; set; }
        }

        public class InsertBanco
        {
            public string? Descricao { get; set; }
            public string? Codigo { get; set; }
        }

        public class UpdateBanco
        {
            public Guid? BancoId { get; set; }
            public string? Descricao { get; set; }
            public string? Codigo { get; set; }
        }
    }
}
