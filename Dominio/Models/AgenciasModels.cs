﻿using AutoMapper;

namespace Dominio.Models
{
    public class AgenciasModels
    {
        [AutoMap(typeof(Dominio.Entidades.Agencias), ReverseMap = true)]
        public class SelectAgencia
        {
            public Guid? AgenciaId { get; set; }
            public Guid? BancoId { get; set; }
            public string? NomeBanco { get; set; }
            public string? CodigoBanco { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroAgencia { get; set; }
            public string? DigitoAgencia { get; set; }
            public DateTime DataAlteracao { get; set; }
        }

        public class InsertAgencia
        {
            public Guid? BancoId { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroAgencia { get; set; }
            public string? DigitoAgencia { get; set; }
        }

        public class UpdateAgencia
        {
            public Guid? AgenciaId { get; set; }
            public Guid? BancoId { get; set; }
            public string? Descricao { get; set; }
            public string? NumeroAgencia { get; set; }
            public string? DigitoAgencia { get; set; }
        }
    }
}
