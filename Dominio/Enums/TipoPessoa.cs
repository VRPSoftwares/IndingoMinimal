﻿using System.ComponentModel;

namespace Dominio.Enums
{
    public enum TipoPessoa : int
    {
        [Description("Pessoa Jurídica")]
        PessoaJuridica = 1,

        [Description("Pessoa Física")]
        PessoaFisica = 2
    }
}
