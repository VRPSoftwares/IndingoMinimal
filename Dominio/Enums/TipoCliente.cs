﻿using System.ComponentModel;

namespace Dominio.Enums
{
    public enum TipoCadastro : int
    {
        [Description("Clinte")]
        Cliente = 1,

        [Description("Fornecedor")]
        Fornecedor = 2
    }
}
