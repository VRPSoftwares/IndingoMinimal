﻿namespace Dominio.Entidades
{
    public class Bancos : EntidadeBase
    {
        public Guid? BancoId { get; set; }
        public string? Descricao { get; set; }
        public string? Codigo { get; set; }
        public List<Agencias> Agencias { get; set; } = [];
    }
}
