﻿namespace Dominio.Entidades
{
    public class PessoaFisica : EntidadeBase
    {
        public Guid? PessoaFisicaId { get; set; }
        public Guid? PessoaId { get; set; }
        public string? Nome { get; set; }
        public string? Apelido { get; set; }
        public DateTime? DataNascimento { get; set; }
        public Pessoas? Pessoa { get; set; }
    }
}
