﻿namespace Dominio.Entidades
{
    public class PessoaJuridica : EntidadeBase
    {
        public Guid? PessoaJuridicaId { get; set; }
        public Guid? PessoaId { get; set; }
        public string? RazaoSocial { get; set; }
        public string? NomeFantasia { get; set; }
        public DateTime? DataFundacao { get; set; }
        public Pessoas? Pessoa { get; set; }
    }
}
