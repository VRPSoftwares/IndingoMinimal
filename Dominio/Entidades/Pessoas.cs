﻿using Dominio.Enums;

namespace Dominio.Entidades
{
    public class Pessoas : EntidadeBase
    {
        public Guid? PessoaId { get; set; }
        public TipoPessoa TipoPessoa { get; set; }
        public TipoCadastro TipoCadastro { get; set; }
        public string? Documento { get; set; }   
        public string? Email { get; set; }
        public PessoaFisica? PessoaFisica { get; set; }
        public PessoaJuridica? PessoaJuridica { get; set; }
    }
}
