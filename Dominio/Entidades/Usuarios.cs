﻿namespace Dominio.Entidades
{
    public class Usuarios : EntidadeBase
    {
        public Guid? UsuarioId { get; set; }
        public string? Nome { get; set; }
        public string? Email { get; set; }
        public string? Senha { get; set; }
        public bool Ativo { get; set; }
    }
}
