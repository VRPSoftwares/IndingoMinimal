﻿namespace Dominio.Entidades
{
    public class ContasBancaria : EntidadeBase
    {
        public Guid? ContaBancariaId { get; set; }
        public Guid? AgenciaId { get; set; }
        public string? Descricao { get; set; }
        public string? NumeroConta { get; set; }
        public string? DigitoConta { get; set; }
        public Agencias? Agencias { get; set; }
    }
}