﻿namespace Dominio.Entidades
{
    public class Agencias : EntidadeBase
    {
        public Guid? AgenciaId { get; set; }
        public Guid? BancoId { get; set; }
        public string? Descricao { get; set; }
        public string? NumeroAgencia { get; set; }
        public string? DigitoAgencia { get; set; }
        public Bancos? Banco { get; set; }
        public List<ContasBancaria> ContasBancarias { get; set; } = [];
    }
}
