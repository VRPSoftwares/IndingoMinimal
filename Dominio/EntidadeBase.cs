﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Dominio
{
    public class EntidadeBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DataCadastro { get; set; } = DateTime.Now;

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DataAlteracao { get; set; } = DateTime.Now;
    }
}
