﻿using Aplicacao.Servicos;
using AutoMapper;
using Dominio.Entidades;
using FluentValidation;
using IndigoMinimal;
using InfraEstrutura.Contextos;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using InfraEstrutura.Repositorios;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Runtime.InteropServices;
using static Dominio.Models.AgenciasModels;
using static Dominio.Models.BancosModels;
using static Dominio.Models.ContasBancariaModels;
using static Dominio.Models.UsuariosModels;

namespace Testes.CadastrosBasicos
{
    [TestFixture]
    public class BancosTeste
    {
        private readonly IAgenciasServicos _iAgenciasServico;
        private readonly IBancosServicos _iBancosServico;
        private readonly IContasBancariaServicos _iContasBancariaServicos;
        private readonly IMapper _mapper;

        public BancosTeste()
        {
            var caminhoJson = String.Concat(Environment.CurrentDirectory, RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? @"\" : @"/", "testsettings.json");
            IConfiguration configuration = new ConfigurationBuilder().AddJsonFile(caminhoJson).Build();

            var service = new ServiceCollection();
            
            service.AddDbContext<DbContext, DbContexto>(options => options.UseInMemoryDatabase(databaseName: "InMemoryDatabase"));

            var options = new DbContextOptionsBuilder<DbContexto>().UseInMemoryDatabase(databaseName: "InMemoryDatabase").Options;
            var context = new DbContexto(options, configuration);

            service.AddSingleton<IConfiguration>(configuration);

            service.AddScoped<IAgenciasRepositorio, AgenciasRepositorio>();
            service.AddScoped<IBancosRepositorio, BancosRepositorio>();
            service.AddScoped<IContasBancariasRepositorio, ContasBancariasRepositorio>();
            service.AddScoped<IUsuariosRepositorios, UsuariosRepositorio>();

            service.AddScoped<IAgenciasServicos, AgenciasServicos>();
            service.AddScoped<IBancosServicos, BancosServicos>();
            service.AddScoped<IContasBancariaServicos, ContasBancariaServicos>();
            service.AddScoped<IUsuariosServicos, UsuariosServicos>();

            service.AddAutoMapper(typeof(MapperConfigs));

            service.AddScoped<IValidator<Agencias>, Aplicacao.Validacoes.Agencias.ValidadorBasico>();
            service.AddScoped<IValidator<InsertAgencia>, Aplicacao.Validacoes.Agencias.ValidadorInsert>();
            service.AddScoped<IValidator<UpdateAgencia>, Aplicacao.Validacoes.Agencias.ValidadorUpdate>();
            service.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Agencias.ValidadorDelete>();

            service.AddScoped<IValidator<Bancos>, Aplicacao.Validacoes.Bancos.ValidadorBasico>();
            service.AddScoped<IValidator<InsertBanco>, Aplicacao.Validacoes.Bancos.ValidadorInsert>();
            service.AddScoped<IValidator<UpdateBanco>, Aplicacao.Validacoes.Bancos.ValidadorUpdate>();
            service.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Bancos.ValidadorDelete>();

            service.AddScoped<IValidator<ContasBancaria>, Aplicacao.Validacoes.ContasBancaria.ValidadorBasico>();
            service.AddScoped<IValidator<InsertContasBancarias>, Aplicacao.Validacoes.ContasBancaria.ValidadorInsert>();
            service.AddScoped<IValidator<UpdateContasBancarias>, Aplicacao.Validacoes.ContasBancaria.ValidadorUpdate>();
            service.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.ContasBancaria.ValidadorDelete>();

            service.AddScoped<IValidator<Usuarios>, Aplicacao.Validacoes.Usuarios.ValidadorBasico>();
            service.AddScoped<IValidator<InsertUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorInsert>();
            service.AddScoped<IValidator<UpdateUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorUpdate>();
            service.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Usuarios.ValidadorDelete>();
            service.AddScoped<IValidator<LoginUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorLogin>();

            var provider = service.BuildServiceProvider(true);
            InicializarBancoDeTeste(context);

            var _iAgenciasRepositorio = provider.GetService<IAgenciasRepositorio>()!;
            var _iBancosRepositorio = provider.GetService<IBancosRepositorio>()!;
            var _iContasBancariasRepositorio = provider.GetService<IContasBancariasRepositorio>()!;

            _iAgenciasServico = provider.GetService<AgenciasServicos>()!;
            _iBancosServico = provider.GetService<BancosServicos>()!;
            _iContasBancariaServicos = provider.GetService<ContasBancariaServicos>()!;

            _mapper = provider.GetService<IMapper>()!;
            
            _iAgenciasServico = new AgenciasServicos(_iAgenciasRepositorio, _iContasBancariasRepositorio, _mapper);
            _iBancosServico = new BancosServicos(_iAgenciasRepositorio, _iBancosRepositorio, _mapper);
        }

        [Test]
        public void Deve_ConsultarTodos()
        {
            Assert.That(_iBancosServico.ObterTodosAsync().Result.Count, Is.EqualTo(2));
        }

        public static void InicializarBancoDeTeste(DbContexto context)
        {
            context.Bancos.AddRange(new List<Bancos>
            {
                new() { BancoId = Guid.Parse("A91CEA93-AC9F-4460-B07E-1D2E401809CE"), Descricao = "Banco Solar", Codigo = "190" },
                new() { BancoId = Guid.Parse("C42C9174-2825-4BEA-AF67-9676A33D4E70"), Descricao = "Banco Luar", Codigo = "091" },
                new() { BancoId = Guid.Parse("7779AB0C-4B0D-41DC-8150-5156181CB972"), Descricao = "Banco Marciano", Codigo = "122" }
            });

            context.Agencias.AddRange(new List<Agencias>
            {
                new() { AgenciaId = Guid.Parse("45B0C497-CD1C-45CD-9B4E-1A5E91E7AB33"), BancoId = Guid.Parse("A91CEA93-AC9F-4460-B07E-1D2E401809CE"), Descricao = "Agência Solar", NumeroAgencia = "6907", DigitoAgencia = "0" },
                new() { AgenciaId = Guid.Parse("FA779CE8-B902-442D-BD5B-94EC1A26056D"), BancoId = Guid.Parse("C42C9174-2825-4BEA-AF67-9676A33D4E70"), Descricao = "Agência Luar", NumeroAgencia = "1758", DigitoAgencia = "7" }
            });

            context.SaveChanges();
        }
    }
}
