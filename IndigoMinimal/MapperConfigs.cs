﻿using AutoMapper;
using Dominio.Entidades;
using static Dominio.Models.AgenciasModels;
using static Dominio.Models.BancosModels;
using static Dominio.Models.ContasBancariaModels;
using static Dominio.Models.UsuariosModels;

namespace IndigoMinimal
{
    public class MapperConfigs : Profile
    {
        public MapperConfigs()
        {
            AgenciasConfigure();
            BancosConfigure();
            ContasBancariasConfigure();
            UsuariosConfigure();
        }

        private void AgenciasConfigure()
        {
            CreateMap<Agencias, SelectAgencia>()
                .ForMember(dest => dest.NomeBanco, opt => opt.MapFrom(src => src.Banco!.Descricao))
                .ForMember(dest => dest.CodigoBanco, opt => opt.MapFrom(src => src.Banco!.Codigo));

            CreateMap<InsertAgencia, Agencias>();
            CreateMap<UpdateAgencia, Agencias>();
        }

        private void BancosConfigure()
        {
            CreateMap<Bancos, SelectBanco>();
            CreateMap<InsertBanco, Bancos>();
            CreateMap<UpdateBanco, Bancos>();
        }

        private void ContasBancariasConfigure()
        {
            CreateMap<ContasBancaria, SelectContasBancarias>()
                .ForMember(dest => dest.Agencia, opt => opt.MapFrom(src => src.Agencias!.Descricao))
                .ForMember(dest => dest.NomeBanco, opt => opt.MapFrom(src => src.Agencias!.Banco!.Descricao))
                .ForMember(dest => dest.BancoId, opt => opt.MapFrom(src => src.Agencias!.Banco!.BancoId))
                .ForMember(dest => dest.CodigoBanco, opt => opt.MapFrom(src => src.Agencias!.Banco!.Codigo));

            CreateMap<InsertContasBancarias, ContasBancaria>();
            CreateMap<UpdateContasBancarias, ContasBancaria>();
        }

        private void UsuariosConfigure()
        {
            CreateMap<Usuarios, SelectUsuario>();
            CreateMap<InsertUsuario, Usuarios>();
            CreateMap<UpdateUsuario, Usuarios>();
            CreateMap<LoginUsuario, Usuarios>();
            CreateMap<Usuarios, LoginUsuarioReturn>();
        }
    }
}
