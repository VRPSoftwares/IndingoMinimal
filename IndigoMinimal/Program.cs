var builder = WebApplication.CreateSlimBuilder(args);
ConfigurationManager configuration = builder.Configuration;

builder.AddDependencyInjections();
builder.AddBasicConfiguration(configuration);
builder.AddFluentValidators();

var app = builder.Build();
app.AddBasicConfiguration();
app.AddEndPoints();
app.Run();