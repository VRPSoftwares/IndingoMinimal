﻿using Dominio.Entidades;
using FluentValidation;
using static Dominio.Models.AgenciasModels;
using static Dominio.Models.BancosModels;
using static Dominio.Models.ContasBancariaModels;
using static Dominio.Models.UsuariosModels;

namespace IndigoMinimal.Extensions
{
    public static class FluentBuilderExtensions
    {
        public static WebApplicationBuilder AddFluentValidators(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IValidator<Agencias>, Aplicacao.Validacoes.Agencias.ValidadorBasico>();
            builder.Services.AddScoped<IValidator<InsertAgencia>, Aplicacao.Validacoes.Agencias.ValidadorInsert>();
            builder.Services.AddScoped<IValidator<UpdateAgencia>, Aplicacao.Validacoes.Agencias.ValidadorUpdate>();
            builder.Services.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Agencias.ValidadorDelete>();

            builder.Services.AddScoped<IValidator<Bancos>, Aplicacao.Validacoes.Bancos.ValidadorBasico>();
            builder.Services.AddScoped<IValidator<InsertBanco>, Aplicacao.Validacoes.Bancos.ValidadorInsert>();
            builder.Services.AddScoped<IValidator<UpdateBanco>, Aplicacao.Validacoes.Bancos.ValidadorUpdate>();
            builder.Services.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Bancos.ValidadorDelete>();

            builder.Services.AddScoped<IValidator<ContasBancaria>, Aplicacao.Validacoes.ContasBancaria.ValidadorBasico>();
            builder.Services.AddScoped<IValidator<InsertContasBancarias>, Aplicacao.Validacoes.ContasBancaria.ValidadorInsert>();
            builder.Services.AddScoped<IValidator<UpdateContasBancarias>, Aplicacao.Validacoes.ContasBancaria.ValidadorUpdate>();
            builder.Services.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.ContasBancaria.ValidadorDelete>();

            builder.Services.AddScoped<IValidator<Usuarios>, Aplicacao.Validacoes.Usuarios.ValidadorBasico>();
            builder.Services.AddScoped<IValidator<InsertUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorInsert>();
            builder.Services.AddScoped<IValidator<UpdateUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorUpdate>();
            builder.Services.AddScoped<IValidator<Guid>, Aplicacao.Validacoes.Usuarios.ValidadorDelete>();
            builder.Services.AddScoped<IValidator<LoginUsuario>, Aplicacao.Validacoes.Usuarios.ValidadorLogin>();

            return builder;
        }
    }
}
