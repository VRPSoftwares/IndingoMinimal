﻿using Hangfire;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IndigoMinimal.Extensions
{
    public static class BuilderExtensions
    {
        private static readonly string MyAllowSpecificOrigins = "AllowAnyOrigin";

        public static WebApplicationBuilder AddDependencyInjections(this WebApplicationBuilder builder)
        {
            builder.Services.AddSingleton<IConfiguration>(builder.Configuration);
            return builder;
        }

        public static WebApplicationBuilder AddBasicConfiguration(this WebApplicationBuilder builder, ConfigurationManager configuration)
        {
            builder.Services.AddDbContext<DbContext, DbContexto>(options => options.UseSqlServer(configuration.GetConnectionString("SQLServer")));
            builder.Services.AddHangfire(x => x.UseSqlServerStorage(configuration.GetConnectionString("SQLServer")));

            RecreateDatabaseIfNotExists(configuration);

            builder.Services.AddHangfireServer(options =>
            {
                options.WorkerCount = Environment.ProcessorCount * 2;
                options.HeartbeatInterval = new TimeSpan(0, 1, 0);
                options.ServerCheckInterval = new TimeSpan(0, 1, 0);
                options.SchedulePollingInterval = new TimeSpan(0, 1, 0);
                options.ServerName = builder.Environment.IsDevelopment() ? "IndigoMinmal Debug" : "IndigoMinmal HangFire";
                options.Queues = builder.Environment.IsDevelopment() ? ["develop"] : ["production"];
            });

            builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options =>
            {
                options.SerializerOptions.PropertyNamingPolicy = null;
            });

            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  policy =>
                                  {
                                      policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                                  });
            });

            builder.Services.AddResponseCaching(options =>
            {
                options.MaximumBodySize = 1024;
                options.UseCaseSensitivePaths = true;
            });
            
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1", Description = "API do sistema" });
            });

            builder.Services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
            });

            builder.Services.AddDistributedMemoryCache();

            builder.Services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            builder.Services.AddSingleton<IConfiguration>(configuration);
            builder.Services.AddAutoMapper(typeof(MapperConfigs));

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ClockSkew = TimeSpan.Zero,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Jwt:Issuer"],
                    ValidAudience = configuration["Jwt:Audience"],
                    LifetimeValidator = TokenLifetimeValidator.Validate,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]!))
                };
            });

            builder.Services.AddAuthorization();

            builder.Services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = c =>
                {
                    var errors = string.Join('\n', c.ModelState.Values.Where(v => v.Errors.Count > 0)
                      .SelectMany(v => v.Errors)
                      .Select(v => v.ErrorMessage));

                    return new BadRequestObjectResult(new
                    {
                        ErrorCode = "600",
                        Message = errors
                    });
                };
            });

            AddInjectionDependencyServicos(builder);
            AddInjectionDependencyRepository(builder);

            return builder;
        }

        public static class TokenLifetimeValidator
        {
            public static bool Validate(DateTime? notBefore, DateTime? expires, SecurityToken tokenToValidate, TokenValidationParameters @param)
            {
                return (expires != null && expires > DateTime.UtcNow);
            }
        }

        private static void RecreateDatabaseIfNotExists(ConfigurationManager configuration)
        {
            using (var context = new InfraEstrutura.Contextos.DbContexto(new(), configuration))
            {
                if (!context.Database.CanConnect())
                {
                    try
                    {
                        context.Database.Migrate();
                    }
                    catch
                    {
                        throw new Exception("Não foi possível estabelecer conexão e/ou criar banco de dados");
                    }
                }
            }
        }

        private static WebApplicationBuilder AddInjectionDependencyServicos(this WebApplicationBuilder builder)
        {
            builder.Services.AddTransient<IAgenciasServicos, AgenciasServicos>();
            builder.Services.AddTransient<IBancosServicos, BancosServicos>();
            builder.Services.AddTransient<IContasBancariaServicos, ContasBancariaServicos>();
            builder.Services.AddTransient<IUsuariosServicos, UsuariosServicos>();

            return builder;
        }

        private static WebApplicationBuilder AddInjectionDependencyRepository(this WebApplicationBuilder builder)
        {
            builder.Services.AddTransient<IAgenciasRepositorio, AgenciasRepositorio>();
            builder.Services.AddTransient<IBancosRepositorio, BancosRepositorio>();
            builder.Services.AddTransient<IContasBancariasRepositorio, ContasBancariasRepositorio>();
            builder.Services.AddTransient<IUsuariosRepositorios, UsuariosRepositorio>();

            return builder;
        }       
    }
}
