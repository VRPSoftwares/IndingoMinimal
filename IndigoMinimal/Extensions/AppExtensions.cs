﻿using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace IndigoMinimal.Extensions
{
    public static class AppExtensions
    {
        public static WebApplication AddBasicConfiguration(this WebApplication app)
        {
            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseHsts();

            app.UseHangfireDashboard("/jobs", new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            });

            app.UseHttpsRedirection();
            app.UseSession();
            app.UseAuthentication();
            app.UseCors("AllowAnyOrigin");
            app.UseAuthorization();
            app.UseDeveloperExceptionPage();
            
            return app;
        }

        public static WebApplication AddEndPoints(this WebApplication app)
        {
            Routes.AgenciasEndPoints.Configure(app);
            Routes.BancosEndPoints.Configure(app);
            Routes.ContasBancariasEndPoints.Configure(app);
            Routes.UsuariosEndPoints.Configure(app);

            return app;
        }
    }

    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}
