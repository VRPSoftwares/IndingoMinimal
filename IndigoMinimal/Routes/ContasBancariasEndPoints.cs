﻿using Microsoft.AspNetCore.Authorization;
using static Dominio.Models.ContasBancariaModels;

namespace IndigoMinimal.Routes
{
    public static class ContasBancariasEndPoints
    {
        public static void Configure(this WebApplication app)
        {
            var Api = app.MapGroup("/Cadastros").WithTags("ContasBancarias");
            {
                Api.MapGet("ContasBancarias", [Authorize] [ValidateUserAndAnchor]
                    async (IContasBancariaServicos contasBancariaServicos) =>
                    {
                        var data = await contasBancariaServicos.ObterTodosAsync();
                        return data.Any() ? Results.Ok(data) : Results.NoContent();
                    }
                );

                Api.MapGet("ContasBancarias/{id}", [Authorize] [ValidateUserAndAnchor]
                    async (IContasBancariaServicos contasBancariaServicos, Guid id) =>
                    {
                        var data = await contasBancariaServicos.ObterAsync(id);
                        return data is null ? Results.NoContent() : Results.Ok(data);
                    }
                );

                Api.MapPost("ContasBancarias", [Authorize] [ValidateUserAndAnchor]
                    async (IContasBancariaServicos contasBancariaServicos, [FromBody] InsertContasBancarias objeto) =>
                    {
                        var inserted = await contasBancariaServicos.CriarAsync(objeto);
                        return inserted.Item2 is null ? Results.Ok(inserted.Item1) : Results.BadRequest();
                    }
                );

                Api.MapPut("ContasBancarias", [Authorize] [ValidateUserAndAnchor]
                    async (IContasBancariaServicos contasBancariaServicos, [FromBody] UpdateContasBancarias objeto) =>
                    {
                        var updated = await contasBancariaServicos.AtualizarAsync(objeto);
                        return updated.Item2 is null ? Results.Ok(updated.Item1) : Results.NoContent();
                    }
                );

                Api.MapDelete("ContasBancarias/{id}", [Authorize] [ValidateUserAndAnchor]
                    async (IContasBancariaServicos contasBancariaServicos, Guid id) =>
                    {
                        var deleted = await contasBancariaServicos.RemoverAsync(id);
                        return deleted.Item2 is null ? Results.Ok(true) : Results.BadRequest();
                    }
                );
            }
        }
    }
}
