﻿using Microsoft.AspNetCore.Authorization;
using static Dominio.Models.AgenciasModels;

namespace IndigoMinimal.Routes
{
    public static class AgenciasEndPoints
    {
        public static void Configure(this WebApplication app)
        {
            var Api = app.MapGroup("/Cadastros").WithTags("Agências");
            {
                Api.MapGet("Agencias", [Authorize] [ValidateUserAndAnchor]
                async (IAgenciasServicos agenciasServicos) =>
                {
                    var data = await agenciasServicos.ObterTodosAsync();
                    return data.Any() ? Results.Ok(data) : Results.NoContent();
                });

                Api.MapGet("Agencias/{id}", [Authorize] [ValidateUserAndAnchor]
                async (IAgenciasServicos agenciasServicos, Guid id) =>
                {
                    var data = await agenciasServicos.ObterAsync(id);
                    return data is null ? Results.NoContent() : Results.Ok(data);
                });

                Api.MapPost("Agencias", [Authorize] [ValidateUserAndAnchor]
                async (IAgenciasServicos agenciasServicos, [FromBody] InsertAgencia objeto) =>
                {
                    var inserted = await agenciasServicos.CriarAsync(objeto);
                    return inserted.Item2 is null ? Results.Ok(inserted.Item1) : Results.BadRequest();
                });

                Api.MapPut("Agencias", [Authorize] [ValidateUserAndAnchor]
                async (IAgenciasServicos agenciasServicos, [FromBody] UpdateAgencia objeto) =>
                {
                    var updated = await agenciasServicos.AtualizarAsync(objeto);
                    return updated.Item2 is null ? Results.Ok(updated.Item1) : Results.NoContent();
                });

                Api.MapDelete("Agencias/{id}", [Authorize] [ValidateUserAndAnchor]
                async (IAgenciasServicos agenciasServicos, Guid id) =>
                {
                    var deleted = await agenciasServicos.RemoverAsync(id);
                    return deleted.Item2 is null ? Results.Ok(true) : Results.BadRequest();
                });
            }
        }
    }
}
