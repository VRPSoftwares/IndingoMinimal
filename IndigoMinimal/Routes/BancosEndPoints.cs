﻿using Microsoft.AspNetCore.Authorization;
using static Dominio.Models.BancosModels;

namespace IndigoMinimal.Routes
{
    public static class BancosEndPoints
    {
        public static void Configure(this WebApplication app)
        {
            var Api = app.MapGroup("/Cadastros").WithTags("Bancos");
            {
                Api.MapGet("Bancos", [Authorize] [ValidateUserAndAnchor] 
                    async (IBancosServicos bancosServicos) =>
                    {
                        var data = await bancosServicos.ObterTodosAsync();
                        return data.Any() ? Results.Ok(data) : Results.NoContent();
                    }
                );

                Api.MapGet("Bancos/{id}", [Authorize] [ValidateUserAndAnchor]
                    async (IBancosServicos bancosServicos, Guid id) =>
                    {
                        var data = await bancosServicos.ObterAsync(id);
                        return data is null ? Results.NoContent() : Results.Ok(data);
                    }
                );

                Api.MapPost("Bancos", [Authorize] [ValidateUserAndAnchor] 
                    async (IBancosServicos bancosServicos, [FromBody] InsertBanco objeto) =>
                    {
                        var inserted = await bancosServicos.CriarAsync(objeto);
                        return inserted.Item2 is null ? Results.Ok(inserted.Item1) : Results.BadRequest();
                    }
                );

                Api.MapPut("Bancos", [Authorize] [ValidateUserAndAnchor] 
                    async (IBancosServicos bancosServicos, [FromBody] UpdateBanco objeto) =>
                    {
                        var updated = await bancosServicos.AtualizarAsync(objeto);
                        return updated.Item2 is null ? Results.Ok(updated.Item1) : Results.NoContent();
                    }
                );

                Api.MapDelete("Bancos/{id}", [Authorize] [ValidateUserAndAnchor] 
                    async (IBancosServicos bancosServicos, Guid id) =>
                    {
                        var deleted = await bancosServicos.RemoverAsync(id);
                        return deleted.Item2 is null ? Results.Ok(true) : Results.BadRequest();
                    }
                );
            }
        }
    }
}
