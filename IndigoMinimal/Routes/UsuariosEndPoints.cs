﻿using Microsoft.AspNetCore.Authorization;
using static Dominio.Models.UsuariosModels;

namespace IndigoMinimal.Routes
{
    public static class UsuariosEndPoints
    {
        public static void Configure(this WebApplication app)
        {
            var Api = app.MapGroup("/Administracao").WithTags("Usuários");
            {
                Api.MapGet("Usuarios", [Authorize] [ValidateUserAndAnchor]
                    async (IUsuariosServicos usuariosServicos) =>
                    {
                        var data = await usuariosServicos.ObterTodosAsync();
                        return data.Any() ? Results.Ok(data) : Results.NoContent();
                    }
                );

                Api.MapGet("Usuarios/{id}", [Authorize] [ValidateUserAndAnchor]
                    async (IUsuariosServicos usuariosServicos, Guid id) =>
                    {
                        var data = await usuariosServicos.ObterAsync(id);
                        return data is null ? Results.NoContent() : Results.Ok(data);
                    }
                );

                Api.MapPost("Usuarios", [Authorize] [ValidateUserAndAnchor]
                    async (IUsuariosServicos usuariosServicos, [FromBody] InsertUsuario objeto) =>
                    {
                        var inserted = await usuariosServicos.CriarAsync(objeto);
                        return inserted.Item2 is null ? Results.Ok(inserted.Item1) : Results.BadRequest();
                    }
                );

                Api.MapPost("Usuarios/CheckLogin", [AllowAnonymous]
                    async (IUsuariosServicos usuariosServicos, LoginUsuario login) =>
                    {
                        var loginReturn = await usuariosServicos.CheckLoginAsync(login);
                        return loginReturn.Item2 is null ? Results.Ok(loginReturn.Item1) : Results.BadRequest(loginReturn.Item2);
                    }
                );

                Api.MapPut("Usuarios", [Authorize] [ValidateUserAndAnchor]
                    async (IUsuariosServicos usuariosServicos, [FromBody] UpdateUsuario objeto) =>
                    {
                        var updated = await usuariosServicos.AtualizarAsync(objeto);
                        return updated.Item2 is null ? Results.Ok(updated.Item1) : Results.NoContent();
                    }
                );

                Api.MapDelete("Usuarios/{id}", [Authorize] [ValidateUserAndAnchor]
                    async (IUsuariosServicos usuariosServicos, Guid id) =>
                    {
                        var deleted = await usuariosServicos.RemoverAsync(id);
                        return deleted.Item2 is null ? Results.Ok(true) : Results.BadRequest();
                    }
                );
            }
        }
    }
}
