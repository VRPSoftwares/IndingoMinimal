﻿using Aplicacao.Validacoes.Agencias;
using AutoMapper;
using Dominio.Entidades;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using System.Linq.Expressions;
using static Dominio.Models.AgenciasModels;

namespace Aplicacao.Servicos
{
    public class AgenciasServicos(IAgenciasRepositorio agenciasRepositorio, 
                                  IContasBancariasRepositorio contasBancariasRepositorio, 
                                  IMapper mapper) : IAgenciasServicos
    {
        private readonly IAgenciasRepositorio _agenciasRepositorio = agenciasRepositorio ?? throw new ArgumentNullException(nameof(agenciasRepositorio));
        private readonly IMapper _mapper = mapper;

        private readonly ValidadorBasico validadorBasico = new();
        private readonly ValidadorInsert validadorInsert = new(agenciasRepositorio);
        private readonly ValidadorUpdate validadorUpdate = new(agenciasRepositorio);
        private readonly ValidadorDelete validadorDelete = new(agenciasRepositorio, contasBancariasRepositorio);

        public async Task<IEnumerable<SelectAgencia>> ObterTodosAsync()
        {
            return _mapper.Map<IEnumerable<SelectAgencia>>(await _agenciasRepositorio.ObterTodosAsync());
        }

        public async Task<SelectAgencia> ObterAsync(Guid Id)
        {
            return _mapper.Map<SelectAgencia>(await _agenciasRepositorio.ObterAsync(Id));
        }

        public async Task<IEnumerable<SelectAgencia>> ObterFiltroAsync(Expression<Func<Agencias, bool>> filtro)
        {
            return _mapper.Map<IEnumerable<SelectAgencia>>(await _agenciasRepositorio.ObterFiltroAsync(filtro));
        }

        public async Task<(SelectAgencia, IEnumerable<string>)> CriarAsync(InsertAgencia insert)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Agencias>(insert));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorInsert.ValidateAsync(insert);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Agencias>(insert);
            return (_mapper.Map<SelectAgencia>(await _agenciasRepositorio.CriarAsync(dados)), null!);
        }

        public async Task<(SelectAgencia, IEnumerable<string>)> AtualizarAsync(UpdateAgencia update)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Agencias>(update));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorUpdate.ValidateAsync(update);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Agencias>(update);
            return (_mapper.Map<SelectAgencia>(await _agenciasRepositorio.AtualizarAsync(dados)), null!);
        }

        public async Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id)
        {
            var result = await validadorDelete.ValidateAsync(Id);
            if (!result.IsValid)
                return (false!, GlobalServicos.RethrowException2(result.Errors));

            return (true, null!);
        }
    }
}
