﻿using Aplicacao.Validacoes.Bancos;
using AutoMapper;
using Dominio.Entidades;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using System.Linq.Expressions;
using static Dominio.Models.BancosModels;

namespace Aplicacao.Servicos
{
    public class BancosServicos(IAgenciasRepositorio agenciasRepositorio, 
                                IBancosRepositorio bancosRepositorio, 
                                IMapper mapper) : IBancosServicos
    {
        private readonly IBancosRepositorio _bancosRepositorio = bancosRepositorio ?? throw new ArgumentNullException(nameof(bancosRepositorio));
        private readonly IMapper _mapper = mapper;

        private readonly ValidadorBasico validadorBasico = new();
        private readonly ValidadorInsert validadorInsert = new(bancosRepositorio);
        private readonly ValidadorUpdate validadorUpdate = new(bancosRepositorio);
        private readonly ValidadorDelete validadorDelete = new(bancosRepositorio, agenciasRepositorio);

        public async Task<IEnumerable<SelectBanco>> ObterTodosAsync()
        {
            return _mapper.Map<IEnumerable<SelectBanco>>(await _bancosRepositorio.ObterTodosAsync());
        }

        public async Task<SelectBanco> ObterAsync(Guid Id)
        {
            return _mapper.Map<SelectBanco>(await _bancosRepositorio.ObterAsync(Id));
        }

        public async Task<IEnumerable<SelectBanco>> ObterFiltroAsync(Expression<Func<Bancos, bool>> filtro)
        {
            return _mapper.Map<IEnumerable<SelectBanco>>(await _bancosRepositorio.ObterFiltroAsync(filtro));
        }

        public async Task<(SelectBanco, IEnumerable<string>)> CriarAsync(InsertBanco insert)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Bancos>(insert));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorInsert.ValidateAsync(insert);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Bancos>(insert);
            return (_mapper.Map<SelectBanco>(await _bancosRepositorio.CriarAsync(dados)), null!);
        }

        public async Task<(SelectBanco, IEnumerable<string>)> AtualizarAsync(UpdateBanco update)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Bancos>(update));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorUpdate.ValidateAsync(update);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Bancos>(update);
            return (_mapper.Map<SelectBanco>(await _bancosRepositorio.AtualizarAsync(dados)), null!);
        }

        public async Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id)
        {
            var result = await validadorDelete.ValidateAsync(Id);
            if (!result.IsValid)
                return (false, GlobalServicos.RethrowException2(result.Errors));

            return (true, null!);
        }
    }
}
