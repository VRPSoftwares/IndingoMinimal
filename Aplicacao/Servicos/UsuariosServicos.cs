﻿using Aplicacao.Validacoes.Usuarios;
using AutoMapper;
using Dominio.Entidades;
using InfraEstrutura.Framework;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using Microsoft.Extensions.Configuration;
using System.Linq.Expressions;
using static Dominio.Models.UsuariosModels;

namespace Aplicacao.Servicos
{
    public class UsuariosServicos(IConfiguration configuration, IUsuariosRepositorios usuariosRepositorio, IMapper mapper) : IUsuariosServicos
    {
        private readonly IConfiguration _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        private readonly IUsuariosRepositorios _usuariosRepositorio = usuariosRepositorio ?? throw new ArgumentNullException(nameof(usuariosRepositorio));
        private readonly IMapper _mapper = mapper;

        private readonly ValidadorBasico validadorBasico = new();
        private readonly ValidadorInsert validadorInsert = new(usuariosRepositorio);
        private readonly ValidadorUpdate validadorUpdate = new(usuariosRepositorio);
        private readonly ValidadorDelete validadorDelete = new();
        private readonly ValidadorLogin validadorLogin = new(usuariosRepositorio);

        public async Task<IEnumerable<SelectUsuario>> ObterTodosAsync()
        {
            return _mapper.Map<IEnumerable<SelectUsuario>>(await _usuariosRepositorio.ObterTodosAsync());
        }

        public async Task<SelectUsuario> ObterAsync(Guid Id)
        {
            return _mapper.Map<SelectUsuario>(await _usuariosRepositorio.ObterAsync(Id));
        }

        public async Task<IEnumerable<SelectUsuario>> ObterFiltroAsync(Expression<Func<Usuarios, bool>> filtro)
        {
            return _mapper.Map<IEnumerable<SelectUsuario>>(await _usuariosRepositorio.ObterFiltroAsync(filtro));
        }

        public async Task<(SelectUsuario, IEnumerable<string>)> CriarAsync(InsertUsuario insert)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Usuarios>(insert));
            if (!result.IsValid) 
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorInsert.ValidateAsync(insert);
            if (!result.IsValid) 
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Usuarios>(insert);
            dados.Senha = BasicFunctions.GenerateRandomPassword(16);
            return (_mapper.Map<SelectUsuario>(await _usuariosRepositorio.CriarAsync(dados)), null!);
        }

        public async Task<(SelectUsuario, IEnumerable<string>)> AtualizarAsync(UpdateUsuario update)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Usuarios>(update));
            if (!result.IsValid) 
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorUpdate.ValidateAsync(update);
            if (!result.IsValid) 
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Usuarios>(update);
            return (_mapper.Map<SelectUsuario>(await _usuariosRepositorio.AtualizarAsync(dados)), null!);
        }

        public async Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id)
        {
            var result = await validadorDelete.ValidateAsync(Id);
            if (!result.IsValid) 
                return (false!, GlobalServicos.RethrowException2(result.Errors));

            await _usuariosRepositorio.RemoverAsync(Id);
            return (true, null!);
        }

        public async Task<(LoginUsuarioReturn, IEnumerable<string>)> CheckLoginAsync(LoginUsuario login)
        {
            var result = await validadorLogin.ValidateAsync(login);
            if (!result.IsValid) 
                return (null!, GlobalServicos.RethrowException2(result.Errors));
            
            Expression<Func<Usuarios, bool>> filtro = x => x.Email == login.Email;
            var dadoPuro = await _usuariosRepositorio.ObterFiltroAsync(filtro);

            var dado = new LoginUsuarioReturn()
            {
                Email = dadoPuro.FirstOrDefault()!.Email,
                Nome = dadoPuro.FirstOrDefault()!.Nome,
                Token = TokenFunctions.GerarTokenJwt(_configuration, (Guid)dadoPuro.FirstOrDefault()!.UsuarioId!, dadoPuro.FirstOrDefault()!.Nome!, dadoPuro.FirstOrDefault()!.Email!),
                UsuarioId = dadoPuro.FirstOrDefault()!.UsuarioId.ToString()
            };

            return (dado, null!);
        }
    }
}
