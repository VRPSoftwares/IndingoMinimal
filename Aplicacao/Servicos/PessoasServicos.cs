﻿using Aplicacao.Validacoes.Pessoas;
using AutoMapper;
using Dominio.Entidades;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using System.Linq.Expressions;
using static Dominio.Models.PessoasModels;

namespace Aplicacao.Servicos
{
    public class PessoasServicos(IPessoasRepositorio pessoasRepositorio, IMapper mapper) : IPessoasServicos
    {
        private readonly IPessoasRepositorio _pessoasRepositorio = pessoasRepositorio ?? throw new ArgumentNullException(nameof(pessoasRepositorio));
        private readonly IMapper _mapper = mapper;

        private readonly ValidadorBasico validadorBasico = new();
        private readonly ValidadorInsert validadorInsert = new(pessoasRepositorio);
        private readonly ValidadorUpdate validadorUpdate = new(pessoasRepositorio);
        private readonly ValidadorDelete validadorDelete = new(pessoasRepositorio);

        public async Task<IEnumerable<SelectPessoas>> ObterTodosAsync()
        {
            return _mapper.Map<IEnumerable<SelectPessoas>>(await _pessoasRepositorio.ObterTodosAsync());
        }

        public async Task<SelectPessoas> ObterAsync(Guid Id)
        {
            return _mapper.Map<SelectPessoas>(await _pessoasRepositorio.ObterAsync(Id));
        }

        public async Task<IEnumerable<SelectPessoas>> ObterFiltroAsync(Expression<Func<Pessoas, bool>> filtro)
        {
            return _mapper.Map<IEnumerable<SelectPessoas>>(await _pessoasRepositorio.ObterFiltroAsync(filtro));
        }

        public async Task<(SelectPessoas, IEnumerable<string>)> CriarAsync(InsertPessoas insert)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Pessoas>(insert));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorInsert.ValidateAsync(insert);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Pessoas>(insert);
            return (_mapper.Map<SelectPessoas>(await _pessoasRepositorio.CriarAsync(dados)), null!);
        }

        public async Task<(SelectPessoas, IEnumerable<string>)> AtualizarAsync(UpdatePessoas update)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<Pessoas>(update));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorUpdate.ValidateAsync(update);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<Pessoas>(update);
            return (_mapper.Map<SelectPessoas>(await _pessoasRepositorio.AtualizarAsync(dados)), null!);
        }

        public async Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id)
        {
            var result = await validadorDelete.ValidateAsync(Id);
            if (!result.IsValid)
                return (false!, GlobalServicos.RethrowException2(result.Errors));

            await _pessoasRepositorio.RemoverAsync(Id);
            return (true, null!);
        }
    }
}
