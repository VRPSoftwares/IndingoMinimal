﻿using Aplicacao.Validacoes.ContasBancaria;
using AutoMapper;
using Dominio.Entidades;
using InfraEstrutura.Interfaces.Repositorios;
using InfraEstrutura.Interfaces.Servicos;
using System.Linq.Expressions;
using static Dominio.Models.ContasBancariaModels;

namespace Aplicacao.Servicos
{
    public class ContasBancariaServicos(IContasBancariasRepositorio contasBancariasRepositorio,
                                        IMapper mapper) : IContasBancariaServicos
    {
        private readonly IContasBancariasRepositorio _contasBancariasRepositorio = contasBancariasRepositorio ?? throw new ArgumentNullException(nameof(contasBancariasRepositorio));
        private readonly IMapper _mapper = mapper;

        private readonly ValidadorBasico validadorBasico = new();
        private readonly ValidadorInsert validadorInsert = new(contasBancariasRepositorio);
        private readonly ValidadorUpdate validadorUpdate = new(contasBancariasRepositorio);
        private readonly ValidadorDelete validadorDelete = new();

        public async Task<IEnumerable<SelectContasBancarias>> ObterTodosAsync()
        {
            return _mapper.Map<IEnumerable<SelectContasBancarias>>(await _contasBancariasRepositorio.ObterTodosAsync());
        }

        public async Task<SelectContasBancarias> ObterAsync(Guid Id)
        {
            return _mapper.Map<SelectContasBancarias>(await _contasBancariasRepositorio.ObterAsync(Id));
        }

        public async Task<IEnumerable<SelectContasBancarias>> ObterFiltroAsync(Expression<Func<ContasBancaria, bool>> filtro)
        {
            return _mapper.Map<IEnumerable<SelectContasBancarias>>(await _contasBancariasRepositorio.ObterFiltroAsync(filtro));
        }

        public async Task<(SelectContasBancarias, IEnumerable<string>)> CriarAsync(InsertContasBancarias insert)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<ContasBancaria>(insert));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorInsert.ValidateAsync(insert);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<ContasBancaria>(insert);
            return (_mapper.Map<SelectContasBancarias>(await _contasBancariasRepositorio.CriarAsync(dados)), null!);
        }

        public async Task<(SelectContasBancarias, IEnumerable<string>)> AtualizarAsync(UpdateContasBancarias update)
        {
            var result = await validadorBasico.ValidateAsync(_mapper.Map<ContasBancaria>(update));
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            result = await validadorUpdate.ValidateAsync(update);
            if (!result.IsValid)
                return (null!, GlobalServicos.RethrowException2(result.Errors));

            var dados = _mapper.Map<ContasBancaria>(update);
            return (_mapper.Map<SelectContasBancarias>(await _contasBancariasRepositorio.AtualizarAsync(dados)), null!);
        }

        public async Task<(bool, IEnumerable<string>)> RemoverAsync(Guid Id)
        {
            var result = await validadorDelete.ValidateAsync(Id);
            if (!result.IsValid)
                return (false, GlobalServicos.RethrowException2(result.Errors));

            return (true, null!);
        }
    }
}
