﻿using FluentValidation;
using InfraEstrutura.Interfaces.Repositorios;
using System.Linq.Expressions;
using static Dominio.Models.UsuariosModels;

namespace Aplicacao.Validacoes.Usuarios
{
    public class ValidadorBasico : AbstractValidator<Dominio.Entidades.Usuarios>
    {
        public ValidadorBasico()
        {
            RuleFor(x => x.Nome)
                .MaximumLength(100).WithMessage(MensagensErro.NomeTamanhoExcedido)
                .NotEmpty().WithMessage(MensagensErro.NomeNaoPreenchido);

            RuleFor(x => x.Email)
                .MaximumLength(100).WithMessage(MensagensErro.EmailTamanhoExcedido)
                .NotEmpty().WithMessage(MensagensErro.EmailNaoPreenchido);
        }
    }

    public class ValidadorInsert : AbstractValidator<InsertUsuario>
    {
        private readonly IUsuariosRepositorios _usuariosRepositorios;

        public ValidadorInsert(IUsuariosRepositorios usuariosRepositorios)
        {
            _usuariosRepositorios = usuariosRepositorios;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Usuarios, bool>> filtro = x => x.Nome == values.Nome;
                    var dados = await _usuariosRepositorios.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.NomeJaExiste);

                    filtro = x => x.Email == values.Email;
                    dados = await _usuariosRepositorios.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.EmailJaExiste);
                });
        }
    }

    public class ValidadorUpdate : AbstractValidator<UpdateUsuario>
    {
        private readonly IUsuariosRepositorios _usuariosRepositorios;

        public ValidadorUpdate(IUsuariosRepositorios usuariosRepositorios)
        {
            _usuariosRepositorios = usuariosRepositorios;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Usuarios, bool>> filtro = x => x.Nome == values.Nome && x.UsuarioId != values.UsuarioId;
                    var dados = await _usuariosRepositorios.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.NomeJaExiste);

                    filtro = x => x.Email == values.Email && x.UsuarioId != values.UsuarioId;
                    dados = await _usuariosRepositorios.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.EmailJaExiste);
                });
        }
    }

    public class ValidadorDelete : AbstractValidator<Guid>
    {
        //private readonly IUsuariosRepositorios _usuariosRepositorios;

        //public ValidadorDelete(IUsuariosRepositorios usuariosRepositorios)
        //{
        //    _usuariosRepositorios = usuariosRepositorios;

        //    RuleFor(c => c)
        //        .CustomAsync(async (values, context, _) =>
        //        {
                    
        //        });
        //}
    }

    public class ValidadorLogin : AbstractValidator<LoginUsuario>
    {
        private readonly IUsuariosRepositorios _usuariosRepositorios;

        public ValidadorLogin(IUsuariosRepositorios usuariosRepositorios)
        {
            _usuariosRepositorios = usuariosRepositorios;

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage(MensagensErro.LoginNaoPreenchido);

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage(MensagensErro.SenhaNaoPreenchido);

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Usuarios, bool>> filtro = x => x.Email == values.Email && x.Senha == values.Password;
                    var login = await _usuariosRepositorios.ObterFiltroAsync(filtro);

                    if (!login.Any())
                        context.AddFailure(MensagensErro.LoginInvalido);
                    else if (!login.FirstOrDefault()!.Ativo)
                        context.AddFailure(MensagensErro.LoginInativado);
                });
        }
    }
}
