﻿namespace Aplicacao.Validacoes.Usuarios
{
    public static class MensagensErro
    {
        public const string NomeTamanhoExcedido = "Nome do usuário deve conter no máximo 100 caracteres";
        public const string NomeNaoPreenchido = "Código do banco deve ser informado";
        public const string NomeJaExiste = "Já existe um usuário no cadastro com esse nome";

        public const string EmailTamanhoExcedido = "Email do usuário deve conter no máximo 100 caracteres";
        public const string EmailNaoPreenchido = "Email do usuário deve ser informado";
        public const string EmailJaExiste = "Já existe um usuário no cadastro com esse email";

        public const string NaoEncontrado = "O usuário informado não foi localizado no cadastro";
        public const string Utilizado = "O usuário informado esta sendo utilizado em outros lugares do sistema (XXXXX)";

        public const string LoginNaoPreenchido = "Email deve ser informado";
        public const string SenhaNaoPreenchido = "Senha deve ser informada";
        public const string LoginInvalido = "Usuário e/ou Senha informados são inválidos";
        public const string LoginInativado = "Usuário inativado no sistema";
    }
}
