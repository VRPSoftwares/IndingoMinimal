﻿namespace Aplicacao.Validacoes.Agencias
{
    public static class MensagensErro
    {
        public const string DescricaoTamanhoExcedido = "Nome da agência deve conter no máximo 100 caracteres";
        public const string DescricaoNaoPreenchido = "Nome da agência deve ser informada";
        public const string DescricaoJaExiste = "Já existe uma agência no cadastro com esse nome";

        public const string NumeroTamanhoExcedido = "Número da agência deve conter no máximo 10 caracteres";
        public const string NumeroNaoPreenchido = "Número da agência deve ser informada";
        public const string NumeroJaExiste = "Já existe uma agência no cadastro com esse número e banco";

        public const string DigitoTamanhoExcedido = "Digito da agência deve conter no máximo 5 caracteres";

        public const string NaoEncontrado = "A agência informada não foi localizada no cadastro";
        public const string Utilizado = "A agência informada esta sendo utilizada em outros lugares do sistema (Contas Bancárias)";
    }
}
