﻿using FluentValidation;
using InfraEstrutura.Interfaces.Repositorios;
using System.Linq.Expressions;
using static Dominio.Models.AgenciasModels;

namespace Aplicacao.Validacoes.Agencias
{
    public class ValidadorBasico : AbstractValidator<Dominio.Entidades.Agencias>
    {
        public ValidadorBasico()
        {
            RuleFor(x => x.Descricao)
                .MaximumLength(100).WithMessage(MensagensErro.DescricaoTamanhoExcedido)
            .NotEmpty().WithMessage(MensagensErro.DescricaoNaoPreenchido);
            RuleFor(x => x.NumeroAgencia)
                .MaximumLength(10).WithMessage(MensagensErro.NumeroTamanhoExcedido)
            .NotEmpty().WithMessage(MensagensErro.NumeroNaoPreenchido);

            RuleFor(x => x.DigitoAgencia)
                .MaximumLength(5).WithMessage(MensagensErro.DigitoTamanhoExcedido);
        }
    }

    public class ValidadorInsert : AbstractValidator<InsertAgencia>
    {
        private readonly IAgenciasRepositorio _agenciasRepositorio;

        public ValidadorInsert(IAgenciasRepositorio agenciasRepositorio)
        {
            _agenciasRepositorio = agenciasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Agencias, bool>> filtro = x => x.Descricao == values.Descricao;
                    var dados = await _agenciasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DescricaoJaExiste);

                    filtro = x => x.Descricao == values.Descricao && x.BancoId == values.BancoId;
                    var check = await _agenciasRepositorio.ObterFiltroAsync(filtro);

                    if (check.Any())
                        context.AddFailure(MensagensErro.NumeroJaExiste);
                });
        }
    }

    public class ValidadorUpdate : AbstractValidator<UpdateAgencia>
    {
        private readonly IAgenciasRepositorio _agenciasRepositorio;

        public ValidadorUpdate(IAgenciasRepositorio agenciasRepositorio)
        {
            _agenciasRepositorio = agenciasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Agencias, bool>> filtro = x => x.Descricao == values.Descricao && x.AgenciaId != values.AgenciaId;
                    var dados = await _agenciasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DescricaoJaExiste);

                    filtro = x => x.NumeroAgencia == values.NumeroAgencia && x.BancoId != x.BancoId && x.AgenciaId != values.AgenciaId;
                    dados = await _agenciasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.NumeroJaExiste);
                });
        }
    }

    public class ValidadorDelete : AbstractValidator<Guid>
    {
        private readonly IContasBancariasRepositorio _contasBancariasRepositorio;
        private readonly IAgenciasRepositorio _agenciasRepositorio;

        public ValidadorDelete(IAgenciasRepositorio agenciasRepositorio, IContasBancariasRepositorio contasBancariasRepositorio)
        {
            _contasBancariasRepositorio = contasBancariasRepositorio;
            _agenciasRepositorio = agenciasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.ContasBancaria, bool>> filtro = x => x.AgenciaId == values;
                    var check = await _contasBancariasRepositorio.ObterFiltroAsync(filtro);

                    if (check.Any())
                        context.AddFailure(MensagensErro.Utilizado);
                });
        }
    }
}
