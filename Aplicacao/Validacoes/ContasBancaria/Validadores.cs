﻿using FluentValidation;
using InfraEstrutura.Interfaces.Repositorios;
using System.Linq.Expressions;
using static Dominio.Models.ContasBancariaModels;

namespace Aplicacao.Validacoes.ContasBancaria
{
    public class ValidadorBasico : AbstractValidator<Dominio.Entidades.ContasBancaria>
    {
        public ValidadorBasico()
        {
            RuleFor(x => x.Descricao)
                .MaximumLength(100).WithMessage(MensagemErro.DescricaoTamanhoExcedido)
                .NotEmpty().WithMessage(MensagemErro.DescricaoNaoPreenchido);

            RuleFor(x => x.NumeroConta)
                .MaximumLength(10).WithMessage(MensagemErro.NumeroTamanhoExcedido)
                .NotEmpty().WithMessage(MensagemErro.NumeroNaoPreenchido);

            RuleFor(x => x.DigitoConta)
                .MaximumLength(5).WithMessage(MensagemErro.DigitoTamanhoExcedido);
        }
    }

    public class ValidadorInsert : AbstractValidator<InsertContasBancarias>
    {
        private readonly IContasBancariasRepositorio _contasBancariasRepositorio;

        public ValidadorInsert(IContasBancariasRepositorio contasBancariasRepositorio)
        {
            _contasBancariasRepositorio = contasBancariasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.ContasBancaria, bool>> filtro = x => x.Descricao == values.Descricao;
                    var dados = await _contasBancariasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagemErro.DescricaoJaExiste);

                    filtro = x => x.Descricao == values.NumeroConta && x.AgenciaId == values.AgenciaId;
                    var check = await _contasBancariasRepositorio.ObterFiltroAsync(filtro);

                    if (check.Any())
                        context.AddFailure(MensagemErro.NumeroJaExiste);
                });
        }
    }

    public class ValidadorUpdate : AbstractValidator<UpdateContasBancarias>
    {
        private readonly IContasBancariasRepositorio _contasBancariasRepositorio;

        public ValidadorUpdate(IContasBancariasRepositorio contasBancariasRepositorio)
        {
            _contasBancariasRepositorio = contasBancariasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.ContasBancaria, bool>> filtro = x => x.Descricao == values.Descricao && x.ContaBancariaId != values.ContaBancariaId;
                    var dados = await _contasBancariasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagemErro.DescricaoJaExiste);

                    filtro = x => x.Descricao == values.NumeroConta && x.AgenciaId == values.AgenciaId && x.ContaBancariaId != values.ContaBancariaId;
                    var check = await _contasBancariasRepositorio.ObterFiltroAsync(filtro);

                    if (check.Any())
                        context.AddFailure(MensagemErro.NumeroJaExiste);
                });
        }
    }

    public class ValidadorDelete : AbstractValidator<Guid>
    {
        //public ValidadorDelete()
        //{
        //    RuleFor(c => c)
        //        .CustomAsync(async (values, context, _) =>
        //        {

        //        });
        //}
    }
}
