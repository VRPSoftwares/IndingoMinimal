﻿namespace Aplicacao.Validacoes.ContasBancaria
{
    public static class MensagemErro
    {
        public const string DescricaoTamanhoExcedido = "Nome da conta bancária deve conter no máximo 100 caracteres";
        public const string DescricaoNaoPreenchido = "Nome da conta bancária deve ser informada";
        public const string DescricaoJaExiste = "Já existe uma conta bancária no cadastro com esse nome";

        public const string NumeroTamanhoExcedido = "Número da conta bancária deve conter no máximo 10 caracteres";
        public const string NumeroNaoPreenchido = "Número da agconta bancáriaência deve ser informada";
        public const string NumeroJaExiste = "Já existe uma conta bancária no cadastro com esse número e agência";

        public const string DigitoTamanhoExcedido = "Digito da conta bancária deve conter no máximo 5 caracteres";

        public const string NaoEncontrado = "A conta bancária informada não foi localizada no cadastro";
        public const string Utilizado = "A conta bancária informada esta sendo utilizada em outros lugares do sistema (xxxx)";
    }
}
