﻿using FluentValidation;
using InfraEstrutura.Interfaces.Repositorios;
using System.Linq.Expressions;
using static Dominio.Models.BancosModels;

namespace Aplicacao.Validacoes.Bancos
{
    public class ValidadorBasico : AbstractValidator<Dominio.Entidades.Bancos>
    {
        public ValidadorBasico()
        {
            RuleFor(x => x.Codigo)
                .MaximumLength(3).WithMessage(MensagensErro.CodigoTamanhoExcedido)
                .NotEmpty().WithMessage(MensagensErro.CodigoNaoPreenchido);

            RuleFor(x => x.Descricao)
                .MaximumLength(100).WithMessage(MensagensErro.DescricaoTamanhoExcedido)
                .NotEmpty().WithMessage(MensagensErro.DescricaoNaoPreenchido);
        }
    }

    public class ValidadorInsert : AbstractValidator<InsertBanco>
    {
        private readonly IBancosRepositorio _bancosRepositorio;

        public ValidadorInsert(IBancosRepositorio bancosRepositorio)
        {
            _bancosRepositorio = bancosRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Bancos, bool>> filtro = x => x.Descricao == values.Descricao;
                    var dados = await _bancosRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DescricaoJaExiste);

                    filtro = x => x.Codigo == values.Codigo;
                    dados = await _bancosRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.CodigoJaExiste);
                });
        }
    }

    public class ValidadorUpdate : AbstractValidator<UpdateBanco>
    {
        private readonly IBancosRepositorio _bancosRepositorio;

        public ValidadorUpdate(IBancosRepositorio bancosRepositorio)
        {
            _bancosRepositorio = bancosRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Bancos, bool>> filtro = x => x.Descricao == values.Descricao && x.BancoId != values.BancoId;
                    var dados = await _bancosRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DescricaoJaExiste);

                    filtro = x => x.Codigo == values.Codigo && x.BancoId != values.BancoId;
                    dados = await _bancosRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.CodigoJaExiste);
                });
        }
    }

    public class ValidadorDelete : AbstractValidator<Guid>
    {
        private readonly IBancosRepositorio _bancosRepositorio;
        private readonly IAgenciasRepositorio _agenciasRepositorio;

        public ValidadorDelete(IBancosRepositorio bancosRepositorio, IAgenciasRepositorio agenciasRepositorio)
        {
            _bancosRepositorio = bancosRepositorio;
            _agenciasRepositorio = agenciasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    var dados = await _bancosRepositorio.ObterAsync(values)!;

                    Expression<Func<Dominio.Entidades.Agencias, bool>> filtro = x => x.BancoId == dados.BancoId;
                    var check = await _agenciasRepositorio.ObterFiltroAsync(filtro);

                    if (check.Any())
                        context.AddFailure(Agencias.MensagensErro.Utilizado);
                });
        }
    }
}
