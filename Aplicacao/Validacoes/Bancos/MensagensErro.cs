﻿namespace Aplicacao.Validacoes.Bancos
{
    public static class MensagensErro
    {
        public const string CodigoTamanhoExcedido = "Código do banco deve conter no máximo 3 caracteres";
        public const string CodigoNaoPreenchido = "Código do banco deve ser informado";
        public const string CodigoJaExiste = "Já existe um banco no cadastro com esse código";

        public const string DescricaoTamanhoExcedido = "Nome do banco deve conter no máximo 100 caracteres";
        public const string DescricaoNaoPreenchido = "Nome do banco deve ser informado";
        public const string DescricaoJaExiste = "Já existe um banco no cadastro com esse nome";

        public const string NaoEncontrado = "O banco informado não foi localizado no cadastro";
        public const string Utilizado = "O banco informado esta sendo utilizado em outros lugares do sistema (Agência)";
    }
}
