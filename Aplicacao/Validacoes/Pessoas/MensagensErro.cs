﻿namespace Aplicacao.Validacoes.Pessoas
{
    public static class MensagensErro
    {
        public const string DocumentoNaoPreenchido = "DOcumento deve ser informado";
        public const string DocumentoJaExiste = "Já existe uma pessoas no cadastro com esse documento";

        public const string EmailTamanhoExcedido = "Email deve conter no máximo 100 caracteres";
        public const string EmailNaoPreenchido = "Email deve ser informado";
        public const string EmailJaExiste = "Já existe uma pessoa no cadastro com esse email";

        public const string NaoEncontrado = "A pessoa informada não foi localizado no cadastro";
        public const string Utilizado = "A pessoa informada esta sendo utilizado em outros lugares do sistema (Agência)";
    }
}
