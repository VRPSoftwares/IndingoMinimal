﻿using FluentValidation;
using InfraEstrutura.Interfaces.Repositorios;
using System.Linq.Expressions;
using static Dominio.Models.PessoasModels;

namespace Aplicacao.Validacoes.Pessoas
{
    public class ValidadorBasico : AbstractValidator<Dominio.Entidades.Pessoas>
    {
        public ValidadorBasico()
        {
            RuleFor(x => x.Documento)
                .NotEmpty().WithMessage(MensagensErro.DocumentoNaoPreenchido);

            RuleFor(x => x.Email)
                .MaximumLength(100).WithMessage(MensagensErro.EmailTamanhoExcedido)
                .NotEmpty().WithMessage(MensagensErro.EmailNaoPreenchido);
        }
    }

    public class ValidadorInsert : AbstractValidator<InsertPessoas>
    {
        private readonly IPessoasRepositorio _pessoasRepositorio;

        public ValidadorInsert(IPessoasRepositorio pessoasRepositorio)
        {
            _pessoasRepositorio = pessoasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Pessoas, bool>> filtro = x => x.Email == values.Email;
                    var dados = await _pessoasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.EmailJaExiste);

                    filtro = x => x.Documento == values.Documento;
                    dados = await _pessoasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DocumentoJaExiste);
                });
        }
    }

    public class ValidadorUpdate : AbstractValidator<UpdatePessoas>
    {
        private readonly IPessoasRepositorio _pessoasRepositorio;

        public ValidadorUpdate(IPessoasRepositorio pessoasRepositorio)
        {
            _pessoasRepositorio = pessoasRepositorio;

            RuleFor(c => c)
                .CustomAsync(async (values, context, _) =>
                {
                    Expression<Func<Dominio.Entidades.Pessoas, bool>> filtro = x => x.Email == values.Email && x.PessoaId != values.PessoaId;
                    var dados = await _pessoasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.EmailJaExiste);

                    filtro = x => x.Documento == values.Documento && x.PessoaId != values.PessoaId;
                    dados = await _pessoasRepositorio.ObterFiltroAsync(filtro);

                    if (dados.Any())
                        context.AddFailure(MensagensErro.DocumentoJaExiste);
                });
        }
    }

    public class ValidadorDelete : AbstractValidator<Guid>
    {
        private readonly IPessoasRepositorio _pessoasRepositorio;

        public ValidadorDelete(IPessoasRepositorio pessoasRepositorio)
        {
            _pessoasRepositorio = pessoasRepositorio;

            //RuleFor(c => c)
            //    .CustomAsync(async (values, context, _) =>
            //    {
            //        var dados = await _pessoasRepositorio.ObterAsync(values)!;

            //        Expression<Func<Dominio.Entidades.Agencias, bool>> filtro = x => x.BancoId == dados.BancoId;
            //        var check = await _pessoasRepositorio.ObterFiltroAsync(filtro);

            //        if (check.Any())
            //            context.AddFailure(Agencias.MensagensErro.Utilizado);
            //    });
        }
    }
}
