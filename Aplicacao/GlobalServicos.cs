﻿using FluentValidation.Results;

namespace Aplicacao
{
    public static class GlobalServicos
    {
        public static void RethrowException(ValidationResult validationResult)
        {
            if (!validationResult.IsValid)
            {
                var erros = string.Join("\r\n", validationResult.Errors.Select(v => v.ErrorMessage));
                throw new Exception(erros);
            }
        }

        public static IEnumerable<string> RethrowException2(List<ValidationFailure> validationResult)
        {
            List<string> dados = [];

            foreach (ValidationFailure list in validationResult)
                dados.Add(list.ErrorMessage);

            return dados.AsEnumerable();
        }
    }
}
